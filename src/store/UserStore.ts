import { observable, action } from "mobx";

export class UserStore {
    // constructor() {
    //     this.isAuthorized = false;
    // }

    @observable isAuthorized: boolean = false;

    @action
    setAuth = (auth: boolean) => {
        this.isAuthorized = auth;
    }
}