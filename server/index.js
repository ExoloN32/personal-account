const express = require('express')
const app = express()
const { check, validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')
// const fs = require("node:fs/promises")
const auth = require('./auth.midleware')
const fs = require('fs')
const config = require('config')
const jsonParser = express.json()

// app.use(express.static(__dirname + "/public"))

const filePath = "list.json"
const fileUsers = "users.json"

app.post("/api/register",
    jsonParser,
    [
        check('email', 'Не корректный email.').isEmail(),
        check('password', 'Минимальная длинна пароля 6 символов.').isLength({min: 6})
    ],
    async (req, res) => {
    try {
        const errors = validationResult(req)
        if( ! errors.isEmpty() ) {
            return res.status(400).json({errors: errors.array(), message: 'Не корректные регистрационные данные.'})
        }
        const {email, password} = req.body
        const content = await fs.readFile(fileUsers, "utf8")
        const users = JSON.parse(content)
        let have = false
        for(let i=0; i<users.length; i++) {
            if(users[i].email == email) {
                res.status(400).json({message: 'Пользователь уже существует.'})
                have = true
                break
            }
        }
        if(!have) {
            const hashedPassword = await bcript.hash(password, 12)
            users.push({email, password: hashedPassword})
            data = JSON.stringify(users)
            await fs.writeFile(fileUsers, data)
            res.status(201).json({message: 'Пользователь создан.'})
        }
    }
    catch(e) {
        res.status(500).json({message: 'Ошибка регистрации'})
    }
})

app.post("/api/login",
jsonParser,
[
    check('email', 'Не корректный email.').isEmail(),
    check('password', 'Введите пароль.').exists()
],
async (req, res) => {
    try {
        const errors = validationResult(req)
        if( ! errors.isEmpty() ) {
            return res.status(400).json({errors: errors.array(), message: 'Не корректные авторизационные данные.'})
        }
        const {email, password} = req.body
        const content = await fs.readFile(fileUsers, "utf8")
        const users = JSON.parse(content)
        let have = false
        for(let i=0; i<users.length; i++) {
            const isMatch = await bcript.compare(email, users[i].email)
            if(isMatch) {
                const hashedPassword = await bcript.hash(password, 12)
                if(hashedPassword == users[i].password) {
                    const token = jwt.sign({email: user[i].email}, config.get('jwtSecret'), {expiresIn: '24h'})
                    return res.json({token})
                }
                else {
                    return res.status(400).json({message: 'Неправильный пароль.'})
                }
            }
        }
        if(!have) {
            res.status(400).json({message: 'Пользователь не найден.'})
        }
    }
    catch(e) {
        res.status(500).json({message: 'Ошибка регистрации'})
    }
})

app.get("/api/users", auth, async (req, res) => {
    try {       
        const content = await fs.readFile(filePath,"utf8")
        const users = JSON.parse(content)
        res.send(users)
    }
    catch(e) {
        res.status(500).json({message: 'Ошибка авторизации'})
    }
})

app.get("/api/users/:id", auth, async (req, res) => {
    try {
        const id = req.params.id
        const content = await fs.readFile(filePath, "utf8")
        const users = JSON.parse(content)
        let user = null
        for(var i=0; i<users.length; i++) {
            if(users[i].id==id) {
                user = users[i]
                break
            }
        }
        if(user) {
            res.send(user)
        }
        else{
            res.status(404).send()
        }
    }
    catch(e) {
        res.status(500).json({message: 'Ошибка сервера'})
    }
})

app.post("/api/users", auth, jsonParser, async (req, res) => {
    try {
        if(!req.body) return res.sendStatus(400)
        
        const userName = req.body.name
        const userAge = req.body.age
        let user = { name: userName, age: userAge }
        
        let data = await fs.readFile(filePath, "utf8")
        let users = JSON.parse(data)
        
        const id = Math.max.apply(Math,users.map(function(o) { return o.id }))
        user.id = id+1
        users.push(user)
        data = JSON.stringify(users)
        fs.writeFile(filePath, data)
        res.send(user)
    }
    catch(e) {
        res.status(500).json({message: 'Ошибка сервера'})
    }
})

app.delete("/api/users/:id", auth, async (req, res) => {
    try {
        const id = req.params.id
        let data = await fs.readFileSync(filePath, "utf8")
        let users = JSON.parse(data)
        let index = -1
        for(var i=0; i < users.length; i++) {
            if(users[i].id==id) {
                index=i
                break
            }
        }
        if(index > -1) {
            const user = users.splice(index, 1)[0]
            data = JSON.stringify(users)
            fs.writeFile(filePath, data)
            res.send(user)
        }
        else {
            res.status(404).send()
        }
    }
    catch(e) {
        res.status(500).json({message: 'Ошибка сервера'})
    }
})

app.put("/api/users", auth, jsonParser, async (req, res) => {
    try {
        if(!req.body) return res.sendStatus(400)
        
        const userId = req.body.id
        const userName = req.body.name
        const userAge = req.body.age
        
        let data = await fs.readFile(filePath, "utf8")
        const users = JSON.parse(data)
        let user
        for(var i=0; i<users.length; i++) {
            if(users[i].id==userId) {
                user = users[i]
                break
            }
        }
        if(user) {
            user.age = userAge
            user.name = userName
            data = JSON.stringify(users)
            fs.writeFileSync(filePath, data)
            res.send(user)
        }
        else {
            res.status(404).send(user)
        }
    }
    catch(e) {
        res.status(500).json({message: 'Ошибка сервера'})
    }
})

const PORT = config.get('port') || 4000

app.listen(PORT, () => console.log(`Server started at port ${PORT}.`))